import React from 'react';
import './Home.scss';
import TodoForm from './todoForm/TodoForm';
import TodoList from './todoList/TodoList';
import todoState from './todoState';
import filterState from './filters/filterState'
import Filters from './filters/Filters';
import { filtersList } from './filters/Filters';
const Home = () =>  {
  const { todos, addTodo, deleteTodo, changeStatus, isIdBusy } = todoState([]);
  const { filters, changeActiveStatus, getActiveStatus} = filterState(filtersList)


  return (
    <div className="App">
      <h1 className='header-todos'>Todos</h1>
      <TodoForm saveTodo={todoText => {
        const trimmedText = todoText.trim();
        if (trimmedText.length > 0) {
          for (var i = 0 ; i <= todos.length ; i++) {
            if(!isIdBusy(i)){
                addTodo({text: trimmedText, finish: false, deleted: false ,id: i })
            }
          }
        }
      }
    } />

      <TodoList filters={filters} todos={todos} deleteTodo={deleteTodo} changeStatus={changeStatus} getActiveStatus={getActiveStatus}/>
      <Filters filters={filters} changeActiveStatus={changeActiveStatus}/>

    </div>
  );
}

export default Home;
