import React from 'react';
import ListItem from './ListItem/ListItem';
import './TodoList.scss';

const TodoList = ({todos ,deleteTodo, changeStatus, getActiveStatus, filters}) => {
    const activeStatus = filters.filter(el => el.active === true )[0].text
    return(
      <div className='todo-list-root'>
        <div className='todo-list-container' >
          {todos && todos.map((todo, index) => (
            <div key={index}>
                {
                activeStatus === 'All' ?
                  !todo.deleted ?
                  <ListItem todo={todo} index={index} deleteTodo={deleteTodo} changeStatus={changeStatus} activeStatus={activeStatus} />
                  : null
                :
                activeStatus === 'Active' ?
                !todo.finish && !todo.deleted ?
                <ListItem todo={todo} index={index} deleteTodo={deleteTodo} changeStatus={changeStatus} activeStatus={activeStatus} />
                  : null
                :
                activeStatus === 'Completed' ?
                  todo.finish && !todo.deleted ?
                  <ListItem todo={todo} index={index} deleteTodo={deleteTodo} changeStatus={changeStatus} activeStatus={activeStatus} />
                  : null
                :
                activeStatus === 'Deleted' ?
                  todo.deleted ?
                  <ListItem todo={todo} index={index} activeStatus={activeStatus} />
                  : null
                : null
                }
            </div>
          ))}
        </div>
      </div>)
  }
export default TodoList
