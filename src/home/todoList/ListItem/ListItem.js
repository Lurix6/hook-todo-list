import React from 'react';
import './ListItem.scss'
import DELETE from './delete.png'
import Checkbox from '../../../components/checkbox/Checkbox'

const ListItem = ({todo, index, deleteTodo, changeStatus, activeStatus}) => {

  return (
    <div>
      <div className= 'list-item-root'>
          <div className= 'list-item-status-text'>
            <Checkbox checked={todo.finish} type='checkbox' onChange={() => {changeStatus && changeStatus(index)}}/>
            <p>{todo.text}</p>
          </div>
          <img src={DELETE} onClick={() =>{deleteTodo && deleteTodo(index)}} alt='delete'/>
      </div>
    </div>
    )
  }
export default ListItem
