import { useState } from 'react';

export default initialValue => {
  const [todos, setTodos] = useState(initialValue);
  return {
    todos,
    addTodo: todoObj => {
      setTodos([...todos, todoObj]);
    },
    deleteTodo: todoIndex => {
      const newTodos = todos.map(el => {
        if (el.id === todoIndex){
          el = {...el, deleted: true}
        }
        return el;
      })
      setTodos(newTodos);
    },
    changeStatus: todoIndex => {
      const newTodos = todos.map(el => {
        if (el.id === todoIndex){
          el = {...el, finish: !el.finish}
        }
        return el;
      })
      setTodos(newTodos);
    },
    isIdBusy: id => {
      let busy = false
      if (todos.length > 0) {
        for (var i = 0; i <= todos.length; i++) {
          if (i < todos.length) {
            if(todos[i].id === id){
              busy = true;
              break;
            }
          }
        }
      }
      return busy
    }
  };
};
