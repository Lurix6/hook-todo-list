import React from 'react';
import './Filters.scss';
import FilterItem from './FilterItem/FilterItem'

const Filters = ({filters, changeActiveStatus, getActiveStatus}) => {

  return (
    <div className='filters-root'>
      <div className='filter-container'>
        {
          filters.map((el, index) => <FilterItem key={index} changeStatus={changeActiveStatus} icon={el.icon} active={el.active} text={el.text} />)
        }
      </div>
    </div>)
  }

export default Filters

export const filtersList = [
    {
      active: true,
      icon: 'history',
      text: 'All',
    },
    {
      active: false,
      icon: 'favorite',
      text: 'Active',
    },
    {
      active: false,
      icon: 'done_all',
      text: 'Completed',
    },
    {
      active: false,
      icon: 'delete_sweep',
      text: 'Deleted',
    }
  ]
