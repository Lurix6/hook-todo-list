import React from 'react';
import './FilterItem.scss';
import Icon from '@material-ui/core/Icon';
import classNames from 'classnames';

const FilterItem = ({icon, text, active, changeStatus}) => {

  return (
    <div className={classNames('filters-block', {'active': active})} onClick={() => changeStatus(text)}>
      <Icon>{icon}</Icon>
      <p>{text}</p>
    </div>)
  }
export default FilterItem
