import { useState } from 'react';

export default initialValue => {
  const [filters, setFilters] = useState(initialValue);

  return {
    filters,
    changeActiveStatus: status => {
      const newFilters = filters.map(el => {
        if(status === el.text){
          el.active = true;
        }else {
          el.active = false;
        }
        return el
      })
      setFilters(newFilters)
    },
  };
};
