import React, { useState } from 'react';
import InputComponent from '../../components/inputComponent/InputComponent'
import './TodoForm.scss';

const TodoForm = ({ saveTodo }) => {
  const [value, setValue] = useState('');

  return (
    <form className='todoForm' onSubmit={ ev => {
      ev.preventDefault();
      saveTodo(value);
      setValue('');
    }}>
      < InputComponent onChange={ ev => {
        setValue(ev.target.value)
      }} value={value} type="text" placeholder='Додати завдання' size='40' />
    </form>
  )
}

export default TodoForm
