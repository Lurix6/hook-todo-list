import React from 'react';
import './Checkbox.scss';

const Checkbox = ({checked, onChange}) => (
  <div className='checkbox-root'>
    <label>
      <input type="checkbox" className="css-checkbox" checked={checked} onChange={onChange}/>
      <i></i>
    </label>
  </div>
)
export default Checkbox
