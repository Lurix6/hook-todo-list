import React from 'react';
import './InputComponent.scss';
import classNames from 'classnames';

const MAX_SYMBOLS = 100

const InputComponent = (props) => {
  const {
    inputClassName,
    type,
    placeholder,
    disabled,
    value,
    onChange,
    required,
    title,
    onClick,
    min,
    maxLength,
    size,
    error,
  } = props;

  return (
    <div
      className={ classNames('root-input-component') }
    >
      {title && <div className={ classNames('text', props.textClassName) }>{title}:</div>}
      <div
        className={ classNames('input-container', { disabled }) }
      >
        <input
          className={ classNames('input-component', inputClassName, {'error': error, 'input-type-number' : (type === 'number')}) }
          type={ type }
          placeholder={ placeholder }
          disabled={ disabled }
          onChange={ onChange }
          onClick={ onClick }
          required= { required }
          value={ value }
          min ={ min }
          size ={ size }
          maxLength={ maxLength }
        />
      </div>
    </div>
  );
};

InputComponent.defaultProps = {
  className: 'input-component',
  textClassName: 'text',
  type: 'text',
  placeholder: '',
  disabled: false,
  title: '',
  maxLength: MAX_SYMBOLS,
};


export default InputComponent;
